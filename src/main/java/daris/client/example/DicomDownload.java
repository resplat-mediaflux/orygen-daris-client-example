package daris.client.example;

import arc.archive.ArchiveInput;
import arc.archive.ArchiveRegistry;
import arc.mf.client.AuthenticationDetails;
import arc.mf.client.Configuration;
import arc.mf.client.RemoteServer;
import arc.mf.client.ServerClient;
import arc.mf.client.archive.Archive;
import arc.mime.NamedMimeType;
import arc.streams.LongInputStream;
import arc.streams.StreamCopy;
import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

public class DicomDownload {

    public static final String COMMAND = "daris-dicom-download";

    public static final int DEFAULT_QUERY_RESULT_PAGE_SIZE = 100;

    public static final Path DEFAULT_CONFIG_PATH = Paths.get(System.getProperty("user.home"), ".Arcitecta", "mflux.cfg");

    public static final String DEFAULT_SERVER_URL = "https://daris.researchsoftware.unimelb.edu.au:443";

    /**
     * Download DICOM data with authentication token.
     *
     * @param serverUrl DaRIS server URL. For example, https://daris.researchsoftware.unimelb.edu.au
     * @param token     authentication token.
     * @param cids      id(s) of the DaRIS object(s) to download.
     * @param filter    predicate to select DICOM series.
     * @param outputDir output directory.
     * @param overwrite overwrite files if they pre-exist.
     * @param verbose   print messages about the download progress.
     * @throws Throwable exception
     */
    public static void download(URL serverUrl, String token, Collection<String> cids, String filter, Path outputDir, boolean overwrite, boolean verbose) throws Throwable {
        download(serverUrl, new AuthenticationDetails(null, token), cids, filter, outputDir, overwrite, verbose);
    }

    public static void download(URL serverUrl, AuthenticationDetails authenticationDetails, Collection<String> cids, String filter, Path outputDir, boolean overwrite, boolean verbose) throws Throwable {
        // connect to DaRIS server
        RemoteServer server = RemoteServer.create(serverUrl);
        ServerClient.Connection connection = server.open();
        try {
            // authenticate
            connection.connect(authenticationDetails);
            // download dicom data
            for (String cid : cids) {
                download(connection, cid, filter, outputDir, overwrite, verbose);
            }
        } finally {
            connection.close(true);
            server.discard();
        }
    }

    public static void download(ServerClient.Connection connection, String cid, String filter, Path outputDir, boolean overwrite, boolean verbose) throws Throwable {
        String query = String.format("(cid starts with '%s' or cid='%s') and mf-dicom-series has value", cid, cid);
        if (filter != null) {
            query += " and (" + filter + ")";
        }
        long cursorPosition = 1;
        boolean complete = false;
        while (!complete) {
            XmlStringWriter w = new XmlStringWriter();
            w.add("where", query);
            w.add("action", "get-cid");
            w.add("idx", cursorPosition);
            w.add("size", DEFAULT_QUERY_RESULT_PAGE_SIZE);
            XmlDoc.Element result = connection.execute("asset.query", w.document());
            Collection<String> seriesCids = result.values("cid");
            if (seriesCids != null) {
                for (String seriesCid : seriesCids) {
                    downloadSeries(connection, seriesCid, outputDir, overwrite, verbose);
                }
            }
            complete = result.booleanValue("cursor/total/@complete");
            cursorPosition += DEFAULT_QUERY_RESULT_PAGE_SIZE;
        }
    }

    static void downloadSeries(ServerClient.Connection connection, String seriesCid, Path outputDir, boolean overwrite, boolean verbose) throws Throwable {
        Archive.declareSupportForAllTypes();
        Path seriesDir = Paths.get(outputDir.toString(), seriesCid);
        XmlStringWriter w = new XmlStringWriter();
        w.add("cid", seriesCid);
        connection.execute("asset.get", w.document(), null, new ServerClient.OutputConsumer() {
            @Override
            protected void consume(XmlDoc.Element re, LongInputStream in) throws Throwable {
                try {
                    if (verbose) {
                        System.out.printf("downloading DICOM series: %s...%n", seriesCid);
                    }
                    if (!Files.exists(seriesDir)) {
                        Files.createDirectory(seriesDir);
                    }
                    String contentType = in.type() != null ? in.type() : re.value("asset/content/type");
                    ArchiveInput ai = ArchiveRegistry.createInput(in, new NamedMimeType(contentType));
                    try {
                        ArchiveInput.Entry entry;
                        while ((entry = ai.next()) != null) {
                            if (entry.isDirectory()) {
                                continue;
                            }
                            String ename = entry.name();
                            Path outputFile = Paths.get(seriesDir.toString(), ename);
                            if (Files.exists(outputFile) && !overwrite) {
                                if (verbose) {
                                    System.out.printf("skipped file: '%s' already exists.%n", outputFile.toString());
                                }
                                continue;
                            }
                            if (verbose) {
                                System.out.printf("downloading file: '%s' ... ", outputFile.toString());
                            }
                            try (OutputStream os = new BufferedOutputStream(Files.newOutputStream(outputFile))) {
                                StreamCopy.copy(entry.stream(), os);
                            }
                            if (verbose) {
                                System.out.println("downloaded.");
                            }
                            ai.closeEntry();
                        }
                    } finally {
                        ai.close();
                    }
                    if (verbose) {
                        System.out.println();
                    }
                } finally {
                    in.close();
                }
            }
        });
    }

    public static void main(String[] args) throws Throwable {

        Configuration config = null;
        URL serverUrl = null;
        String token = null;
        AuthenticationDetails authenticationDetails = null;
        Path outputDir = Paths.get(System.getProperty("user.dir"));
        boolean overwrite = false;
        boolean verbose = false;
        Set<String> cids = new LinkedHashSet<>();
        String filter = null;

        // parse arguments...
        try {
            for (int i = 0; i < args.length; ) {
                if ("-cfg".equals(args[i])) {
                    Path cfgFile = Paths.get(args[i + 1]);
                    if (!Files.exists(cfgFile)) {
                        throw new IllegalArgumentException("Config file: " + cfgFile + " does not exist.");
                    }
                    config = Configuration.load(cfgFile.toFile(), false);
                    i += 2;
                } else if ("-url".equals(args[i])) {
                    serverUrl = new URL(args[i + 1]);
                    i += 2;
                } else if ("-token".equals(args[i])) {
                    token = args[i + 1];
                    i += 2;
                } else if ("-output".equals(args[i])) {
                    outputDir = Paths.get(args[i + 1]);
                    if (!Files.exists(outputDir) || !Files.isDirectory(outputDir)) {
                        throw new IllegalArgumentException("Output directory: " + outputDir + " does not exist.");
                    }
                    i += 2;
                } else if ("-filter".equals(args[i])) {
                    filter = args[i + 1];
                    i += 2;
                } else if ("-overwrite".equals(args[i])) {
                    overwrite = true;
                    i += 1;
                } else if ("-verbose".equals(args[i])) {
                    verbose = true;
                    i += 1;
                } else {
                    String cid = args[i];
                    if (!cid.matches("^\\d+(\\d*.)*\\d+$")) {
                        throw new IllegalArgumentException("Invalid cid: " + cid);
                    }
                    cids.add(cid);
                    i += 1;
                }
            }

            if (serverUrl == null) {
                if (config == null && Files.exists(DEFAULT_CONFIG_PATH)) {
                    config = Configuration.load(DEFAULT_CONFIG_PATH.toFile(), false);
                }
                if (config != null) {
                    String proto = config.transport() != null && !config.transport().name().toLowerCase().startsWith("https") ? "http" : "https";
                    int port = config.port() >= 0 && config.port() < 65535 ? config.port() : (proto.equals("https") ? 443 : 80);
                    serverUrl = new URL(String.format("%s://%s:%d", proto, config.host(), port));
                } else {
                    serverUrl = new URL(DEFAULT_SERVER_URL);
                }
            }

            if (token != null) {
                authenticationDetails = new AuthenticationDetails(null, token);
            } else {
                if (config != null && config.haveCredentials()) {
                    authenticationDetails = config.authenticationDetails();
                } else {
                    throw new IllegalArgumentException("Missing token.");
                }
            }

            if (cids.isEmpty()) {
                throw new IllegalArgumentException("Missing cid.");
            }
        } catch (Throwable e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
            printUsage();
            System.exit(1);
        }

        // download dicom data
        download(serverUrl, authenticationDetails, cids, filter, outputDir, overwrite, verbose);
    }


    private static void printUsage() {
        System.out.println();
        System.out.println("USAGE:");
        System.out.printf("  %s [OPTIONS] CID...%n", COMMAND);
        System.out.println();
        System.out.println("POSITIONAL ARGUMENTS:");
        System.out.println("    CID        Citeable identifiers of the DaRIS subject/study/dataset to download.");
        System.out.println();
        System.out.println("OPTIONS:");
        System.out.println("    -cfg <config file>");
        System.out.println("        Config file contains DaRIS server and authentication details.");
        System.out.println();
        System.out.println("    -url <proto://host:port>");
        System.out.println("        DaRIS server URL. For example: " + DEFAULT_SERVER_URL);
        System.out.println();
        System.out.println("    -token <token>");
        System.out.println("        Token to authenticate with DaRIS server.");
        System.out.println();
        System.out.println("    -filter <query>");
        System.out.println("        AQL filter to select DICOM series within the specified subject/study.");
        System.out.println();
        System.out.println("    -output <directory>");
        System.out.println("        Output directory. Defaults to current working directory.");
        System.out.println();
        System.out.println("    -overwrite");
        System.out.println("        Overwrite if the files already exist at destination.");
        System.out.println();
        System.out.println("    -verbose");
        System.out.println("        Verbose mode.  Print messages about the download progress.");
        System.out.println();
    }
}
