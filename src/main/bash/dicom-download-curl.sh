#!/bin/bash

#
# This is an example todownload DICOM data from DaRIS using curl.
# You need to modify URL, TOKEN, PROJECT_CID or FILTER to fit
# your requirements.
#

# DaRIS DICOM download servlet URL:
URL=https://daris.researchsoftware.unimelb.edu.au/daris/dicom.mfjp

# DaRIS auth token
TOKEN=XXXXXXXXXX

# DaRIS project id is 1.8.8
PROJECT_CID=1.8.8

# select DICOM T2 series uploaded within last 7 days
FILTER="xpath(mf-dicom-series/description) like '.*T2.*' and mtime>='today-7day'"


curl -G -o ${PROJECT_CID}-download.zip --data-urlencode "module=download" --data-urlencode "_token=${TOKEN}" --data-urlencode "cid=${PROJECT_CID}" --data-urlencode "filter=${FILTER}" ${URL}